import requests

def get_weather(key='5e7e50b7ab514554ab5122841232409', city='Moscow'):
    url = 'http://api.weatherapi.com/v1/current.json'
    params = {
        'key': key,
        'q': city
    }
    response = requests.get(url, params=params)
    result = response.json()
    if 'error' in result:
        raise RuntimeError(f'Error from API: {result["error"]}')
    return result
